import React, { useState } from 'react';
import './App.css';

function App() {
  const [leftSide, setLeftSide] = useState([
    { id: 1, name: 'Item 1', checked: false },
    { id: 2, name: 'Item 2', checked: false },
    { id: 3, name: 'Item 3', checked: false },
  ]);
  const [rightSide, setRightSide] = useState([]);

  const toggleChecked = (id) => {
    const updatedLeftSide = leftSide.map(item =>
      item.id === id ? { ...item, checked: !item.checked } : item);
    setLeftSide(updatedLeftSide);
  };

  const moveToRight = () => {
    const checkedItems = leftSide.filter(item => item.checked);
    const updatedLeftSide = leftSide.filter(item => !item.checked);
    setLeftSide(updatedLeftSide);
    setRightSide([...rightSide, ...checkedItems]);
  };
  const moveToLeft = () => {
    const checkedItems = rightSide.filter(item => item.checked);
    const updatedRightSide = rightSide.filter(item => !item.checked);
    setRightSide(updatedRightSide);
    setLeftSide([...leftSide, ...checkedItems]);
  };

  const moveAllToRight = () => {
    setRightSide([...rightSide, ...leftSide]);
    setLeftSide([]);
  };

  const moveAllToLeft = () => {
    setLeftSide([...leftSide, ...rightSide]);
    setRightSide([]);
  };

  return (
    <div style={{ display: 'flex', border: "1px solid", width: 600, height: 600, justifyContent: 'space-between', alignItems: 'center' }}>
      <div style={{ border: "1px solid", width: 200, height: 400 }}>
        <ul>
          {leftSide.map((leftItem) => (
            <li key={leftItem.id}>
              {leftItem.name}
              <input type="checkbox" checked={leftItem.checked} onChange={() => toggleChecked(leftItem.id)} />
            </li>
          ))}
        </ul>
      </div>
      <div>
        <button onClick={moveToRight}>Move to Right</button>
        <button onClick={moveAllToRight}>All to Right</button>
        <button onClick={moveAllToLeft}>All to Left</button>
        <button onClick={moveToLeft}> Move to Left</button>
      </div>
      <div style={{ border: "1px solid", width: 200, height: 400 }}>
        <ul>
          {rightSide.map((rightItem) => (
            <li key={rightItem.id}>
              {rightItem.name}
              <input type="checkbox" checked={rightSide.checked} onChange={() => toggleChecked(leftItem.id)} />

            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}

export default App;
